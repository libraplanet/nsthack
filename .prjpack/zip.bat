@SETLOCAL
SET L_FILE=nsthack_src-1.0.zip
CD %~dp0
DEL /S /Q /F %L_FILE%

7za a -tzip -mx=9 %L_FILE% .\bin\* -r

@ENDLOCAL