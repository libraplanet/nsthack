@SETLOCAL
@GOTO :l_main_proc


:l_sub_copy_prj
@ECHO %1
@REM XCOPY /Y /F /E /I ..\..\%1\* .\%1\
MKDIR .\%1
COPY /B /Y ..\..\%1\%1.c .\%1\
COPY /B /Y ..\..\%1\%1.def .\%1\
COPY /B /Y ..\..\%1\%1.vcproj .\%1\
@GOTO :EOF

:l_main_proc
@SET TARGET=bin
@CD %~dp0

@RMDIR /S /Q %TARGET%
@MKDIR %TARGET%

@CD %TARGET%

COPY /B /Y ..\..\nsthack.ncb .\
COPY /B /Y ..\..\nsthack.sln .\
COPY /B /Y ..\..\build.bat .\
COPY /B /Y ..\..\readme.txt .\

@CALL :l_sub_copy_prj comdlg32
@CALL :l_sub_copy_prj shell32

XCOPY /Y /F /E /I ..\..\nsthack\src\* .\nsthack\src\
MKDIR .\nsthack\debug\
MKDIR .\nsthack\release\


@ENDLOCAL