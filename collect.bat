@SETLOCAL
@SET TARGET=collect

@RMDIR /S /Q %TARGET%
@MKDIR %TARGET%

@COPY /Y release\shell32.dll		%TARGET%
@COPY /Y release\comdlg32.dll		%TARGET%
@COPY /Y nsthack\src\nsthack.bat	%TARGET%
@COPY /Y readme.txt					%TARGET%

@ENDLOCAL