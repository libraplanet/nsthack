◆概要
    アプリケーション名    nsthack
    バージョン            v1.0
    開発環境              Visual Studio 2005
                          コマンドプロンプト
    開発者                takumi
    連絡先                libraplanet.sky@gmail.com

◆はじめに
    NScripterによって使用するツール群をCUI化する為の環境を提供します。
    この環境を利用する事で、コマンドのみでツールを使用することが出来、
    作業の自動化が可能になります。
    変換作業をバッチ化したいときなどにご使用ください。
    フリーウェアなので、勝手に使ってください。

◆同梱構成
    [./]
      ├nsthack.bat            呼び出しツール(実行スクリプト)
      ├comdlg32.dll           ハックしたAPIを提供する自作comdlg32.dll
      ├shell32.dll            ハックしたAPIを提供する自作shell32.dll
      └readme.txt             このファイルです。

◆動作環境
    Windows XPでのみ動作確認済み

    ※このツールには、NScripterに同梱されている、"nsaarc.exe"と"nscmake.exe"が必要になります。

    ※Windows XPのデフォルトのDLLの呼び出し順番に則り、
      カレントのDLLを最優先で呼び出させるロジックを使用しているため、
      DLLの呼び出し順番を変更している環境では利用できない場合があります。

◆確認済みツール
    ・nsaarc.exe
    ・nscmake.exe

◆注意
    nsthackはツールをコマンドのみでfix出来る環境を提供する物です。
    それ以外の部分ではツールの内部動作に依存します。
    (例えばnsaarc.exeでは日本語ファイル名が使用できない、など。)

    nsthackはNScripterとは関係がありません。
    nsthackを用いての不具合はNScripter開発者様には送らない様お願いいたします。

    当方は、nsthackを用いてのいかなる不具合の責任を追いかねます。
    ご利用は自己責任でお願いいたします。

◆インストール
    任意のフォルダに解凍し、
    nsaarc.exeとnscmake.exeを解凍先にコピーをしてください。

    ※通常の、GUIで使用するツールとの共存は出来ません。
      GUIで使用したいツールとは別のフォルダにインストールし、ツールをコピーしてきてください。

◆アンインストール
    レジストリは使用しません。
    解凍したファイルをそのまま削除してください。

◆使い方
    手順01)
        解凍したnsthack.bat、comdlg32.dll、shell32.dllと同じフォルダに
        nsaarc.exeとnscmake.exeを配置します。

            ├nsthack.bat
            ├comdlg32.dll
            ├shell32.dll
            ├nsaarc.exe        ←コピーする
            ├nscmake.exe       ←コピーする
            ・
            ・
            ・

    手順02)
        nsthack.batに引数を与えて呼び出します。

    ※この時、"nsaarc.exe.local"や"nscmake.exe.local"と言ったファイルが生成されます。
      それらのファイルは必要ファイルであり、無ければ、またツールを呼び出す度に生成されます。

◆引数
    nsthack.bat [EXECUTABLE] [SOURCE DIR] [OUT FILE]
    nsthack.bat [OPTION]

        [EXECUTABLE]        使用するツール(nsaarc.exe, nscmake.exe)
        [SOURCE DIR]        読み込みフォルダ(相対パス可)
        [OUT FILE]          出力ファイル(相対パス可)
        [OPTION]
          /h, /help         ヘルプを表示します
          /v, /version      バージョンを表示します

    例)
        nsthack.bat nsaarc.exe .\in .\out\arc.nsa
        nsthack.bat nscmake.exe .\in .\out\nscript.dat

◆解説
    nsaarc.exe入力フォルダを最初に選択します。
    選択のダイアログにはFolderBrowserDialogが使用されており、このAPIはcomdlg32.dllに存在します。
    このcomdlg32.dll内のFolderBrowserDialogを、ダイアログを表示せず、
    環境変数から取得したパスを返す様に同名のAPIを内包した、同名のcomdlg32.dllを作成します。
    nsaarc.exeがcomdlg32.dllを参照する為にDLLの検索法則を用いて、
    nsaarc.exeと同じフォルダにnsaarc.exe.localファイルを作成し、comdlg32.dllも配置します。
    これによりnsaarc.exeが参照するcomdlg32.dllは、OSのモノではなく、
    カレントにある私の作成したcomdlg32.dllを参照する様になり、
    私の作成したAPIが呼ばれる様になります。
    同様に、出力ファイル選択のSaveFileDialogのAPIはshell32.dllにあり、
    こちらも環境変数から取得したパスを返すAPIをSaveFileDialogとして実装した、
    shell32.dllを作成し、nsaarc.exeと同じフォルダに配置します。
    FolderBrowserDialogなどが受け取る環境変数を設定する様に、
    バッチファイルのnsthack.batを経由する事で、引数のみでの実行を実現しています。

        > nsthack.bat nsaarc.exe .\in .\out\arc.nsa
            ↓
            ↓
        ┏[nsthack.bat ]━━━━━━━━━━━┓      ┏[環境変数]━━━━━━┓
        ┃・".\in"をフルパス化 →→→→→→(書き込み)→→NSTHAC_INPUTDIR←←←←←┐
        ┃・".\out\arc.nsa"をフルパス化→→(書き込み)→→NSTHAC_OUTPUTFILE←←←←↑←┐
        ┃・"nsaarc.exe"をコール              ┃      ┗━━━━━━━━━━━┛  ↑  ↑
        ┗━↓━━━━━━━━━━━━━━━━┛                                  ↑  ↑
            ↓                                                                    ↑  ↑
        ┏[nsaarc.exe]━━━━━━━━━━━━┓                                  ↑  ↑
        ┃・nsaarc.exe.localがある            ┃                                  ↑  ↑
        ┃    ・カレントのcomdlg32.dllを参照  ┃                                  ↑  ↑
        ┃        ・FolderBrowserDialogAPI→(参照)→→→→→→→→→→→→→→→→┘  ↑
        ┃                                    ┃                                      ↑
        ┃    ・カレントのshell32.dllを参照   ┃                                      ↑
        ┃        ・SaveFileDialog→→→→→(参照)→→→→→→→→→→→→→→→→→→┘
        ┃                                    ┃
        ┃・変換                              ┃
        ┗━━━━━━━━━━━━━━━━━━┛

◆更新情報
    090813    v1.0    公開

◆おまけ
    nsaarc.exeなどの様に、
    SHBrowseForFolderAメソッドでフォルダを選択したパスを、
    SHGetPathFromIDListAメソッドでパスを抽出し、
    GetSaveFileNameAメソッドで保存先を選択し、
    あとは内部で処理を行うツールであれば、
    確認済みツール以外でも利用出来る可能性があります。
    nsthack.batでは、第一引数が確認済みツールか否かのチェックは行っておらず、
    カレントにある実行ファイルであれば呼び出しそのものは可能になっております。
    DLLとバッチファイルでの実装になっているので、興味があれば、
    バッチファイルを自由に改変してご利用ください。
