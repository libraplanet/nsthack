#pragma warning(disable: 4141)
#define _CRT_SECURE_NO_WARNINGS
#define DllExport __declspec(dllexport)

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#define L_ENV "NSTHAC_OUTPUTFILE"

/**
 * get output file from env
 * sharing wrapper method.
 */
BOOL getOutputFile(LPOPENFILENAMEA arg)
{
	char *str = getenv(L_ENV);
	if(str == NULL)
	{
		return FALSE;
	}
	else
	{
		sprintf(arg->lpstrFile, str);
		return TRUE;
	}
}

/**
 * GetSaveFileName
 * select output file at dialog. 
 */
DllExport WINCOMMDLGAPI BOOL APIENTRY GetSaveFileNameA(LPOPENFILENAMEA arg)
{
	return getOutputFile(arg);
}

/**
 * GetOpenFileName
 * select output file at dialog. 
 */
DllExport WINCOMMDLGAPI BOOL  APIENTRY GetOpenFileNameA(LPOPENFILENAMEA arg)
{
	return getOutputFile(arg);
}
