@SETLOCAL

@rem //---------------------------------------------------
@rem // set system env.
@rem //---------------------------------------------------
@SET L_PROJ_VERSION=1.0
@SET L_PROJ_AUTHER=takumi

@rem //---------------------------------------------------
@rem // process switch.
@rem //---------------------------------------------------
@IF "%1" == "/h"		GOTO l_view_help
@IF "%1" == "/help"		GOTO l_view_help
@IF "%1" == "/v"		GOTO l_view_version
@IF "%1" == "/version"	GOTO l_view_version
@GOTO l_main_proc

@rem //---------------------------------------------------
@rem // view help.
@rem //---------------------------------------------------
:l_view_help
@CALL :l_out_help
@GOTO l_end

@rem //---------------------------------------------------
@rem // view version.
@rem //---------------------------------------------------
:l_view_version
@CALL :l_out_version
@GOTO l_end

@rem //---------------------------------------------------
@rem // out help.
@rem //---------------------------------------------------
:l_out_help
	@ECHO Usage: nsthack [EXECUTABLE] [SOURCE DIR] [OUT FILE]
	@ECHO   or: nsthack [OPTION]
	@ECHO.
	@ECHO [EXECUTABLE]        'nsaarc.exe' ^| 'nscmake.exe'
	@ECHO [SOURCE DIR]        input source directry path
	@ECHO [OUT FILE]          output file path
	@ECHO [OPTION]
	@ECHO   /h, /help         view help.
	@ECHO   /v, /version      view version.
@GOTO :EOF

@rem //---------------------------------------------------
@rem // out version.
@rem //---------------------------------------------------
:l_out_version
	@ECHO nsthack %L_PROJ_VERSION%
	@ECHO copyright takumi.
@GOTO :EOF

@rem //---------------------------------------------------
@rem // check exe
@rem //---------------------------------------------------
:l_check_exe_0
	@SET L_ERROR_EXE_PATH=0

	@IF %L_ERROR_EXE_PATH% == 0 IF "%~1" == "" (
		@rem not argument exe target.
		@SET L_ERROR_EXE_PATH=1
	) ELSE (
		@rem valuable.
	)

	@IF %L_ERROR_EXE_PATH% == 0 IF NOT EXIST "%~2" (
		@rem exe path is not found.
		@SET L_ERROR_EXE_PATH=2
	) ELSE (
		@rem exe path is exist.
	)

	@IF %L_ERROR_EXE_PATH% == 0 IF EXIST "%~2.\" (
		@rem exe path is directry.
		@SET L_ERROR_EXE_PATH=3
	) ELSE (
		@rem exe path is file.
	)

	@rem ECHO [check exe] result. %L_ERROR_EXE_PATH%

@GOTO :EOF

@rem //---------------------------------------------------
@rem // check input dir
@rem //---------------------------------------------------
:l_check_indir_0
	@rem ECHO [l_check_indir_0]
	@rem ECHO "%~1"
	@rem ECHO "%~2"

	@SET L_ERROR_INPUTDIR=0

	@IF %L_ERROR_INPUTDIR% == 0 IF "%~1" == "" (
		@rem not argument input path.
		@SET L_ERROR_INPUTDIR=1
	) ELSE (
		@rem valuable.
	)

	@IF %L_ERROR_INPUTDIR% == 0 IF NOT EXIST "%~2" (
		@rem input path is not found.
		@SET L_ERROR_INPUTDIR=2
	) ELSE (
		@rem input path is exist.
	)

	@IF %L_ERROR_INPUTDIR% == 0 IF NOT EXIST "%~2.\" (
		@rem input path is not directry.
		@SET L_ERROR_INPUTDIR=3
	) ELSE (
		@rem input path is directry.
	)

	@rem ECHO [check input dir] result. %L_ERROR_INPUTDIR%

@GOTO :EOF

@rem //---------------------------------------------------
@rem // check output file
@rem //---------------------------------------------------
:l_check_outfile_0
	@rem ECHO [l_check_outfile_0]
	@rem ECHO "%~1"
	@rem ECHO "%~2"

	@SET L_ERROR_OUTPUTFILE=0

	@IF %L_ERROR_OUTPUTFILE% == 0 IF "%~1" == "" (
		@rem not argument output path.
		@SET L_ERROR_OUTPUTFILE=1
	) ELSE (
		@rem valuable.
	)

	@IF %L_ERROR_OUTPUTFILE% == 0 IF EXIST "%~2.\" (
		@rem output path is not file.
		@SET L_ERROR_OUTPUTFILE=2
	) ELSE (
		@rem output path is file.
	)

	@rem ECHO [check output file] result. %L_ERROR_OUTPUTFILE%

@GOTO :EOF

@rem //---------------------------------------------------
@rem // creake exe local
@rem //---------------------------------------------------
@:l_create_exelocal
	@rem ECHO [l_create_exelocal]
	@rem ECHO %1
	@rem ECHO "%~2.local"


	@SET L_ERROR_EXE_LOCAL=0

	@IF %L_ERROR_EXE_LOCAL% == 0 IF NOT %1 == 0 (
		@rem exe path error yet.
		@SET L_ERROR_EXE_LOCAL=1
	)

	@IF %L_ERROR_EXE_LOCAL% == 0 IF EXIST "%~2.\" (
		@rem exe.local is directry
		@SET L_ERROR_EXE_LOCAL=2
	)

	@IF %L_ERROR_EXE_LOCAL% == 0 IF NOT EXIST "%~2" (
		@rem exe.local is not found.
		@TYPE NUL > "%~2"
	)

	@IF %L_ERROR_EXE_LOCAL% == 0 IF NOT %ERRORLEVEL% == 0 (
		@rem create exe.local file error.
		@SET L_ERROR_EXE_LOCAL=3
	)

	@rem ECHO [creake exe local] result. %L_ERROR_EXE_LOCAL%
@GOTO :EOF

@rem //---------------------------------------------------
@rem // main proc.
@rem //---------------------------------------------------
@:l_main_proc
	@rem create strings at argument.
	@SET L_ARG_EXE_PATH=%~dp0%~1
	@SET L_ARG_INPUTDIR=%~f2
	@SET L_ARG_OUTPUTFILE=%~f3
	@SET L_ARG_EXE_LOCAL=%L_ARG_EXE_PATH%.local

	@rem initialize error state.
	@SET L_ERROR_EXE_PATH=0
	@SET L_ERROR_INPUTDIR=0
	@SET L_ERROR_OUTPUTFILE=0
	@SET L_ERROR_EXE_LOCAL=0

	@rem check argument.
	@CALL :l_check_exe_0		"%~1"	"%L_ARG_EXE_PATH%"
	@CALL :l_check_indir_0		"%~2"	"%L_ARG_INPUTDIR%"
	@CALL :l_check_outfile_0	"%~3"	"%L_ARG_OUTPUTFILE%"

	@ECHO //--------------------------------------------------
	@ECHO //                      nsthack
	@ECHO //          NScripter Toll HACKing consol.
	@ECHO //                             version %L_PROJ_VERSION%
	@ECHO //                             auther  %L_PROJ_AUTHER%
	@ECHO //--------------------------------------------------
	@ECHO [env]
	@ECHO   tool=%L_ARG_EXE_PATH%
	@ECHO   source=%L_ARG_INPUTDIR%
	@ECHO   output=%L_ARG_OUTPUTFILE%
	@ECHO   exe.local=%L_ARG_EXE_LOCAL%

	@ECHO.
	@ECHO [init exe.local]
	@CALL :l_create_exelocal %L_ERROR_EXE_PATH% "%L_ARG_EXE_LOCAL%"

	@rem init exelocal error.
	@IF %L_ERROR_EXE_LOCAL% == 1 (
		@ECHO error. illegal exepath ^(not create^).
	)
	@IF %L_ERROR_EXE_LOCAL% == 2 (
		@ECHO error. exe.local is directry.
	)
	@IF %L_ERROR_EXE_LOCAL% == 3 (
		@ECHO error. create exe.local file error.
	)

	@ECHO.
	@ECHO [call]

	@rem argument exe error.
	@IF %L_ERROR_EXE_PATH% == 1 (
		@ECHO error. not argument exe target.
	)
	@IF %L_ERROR_EXE_PATH% == 2 (
		@ECHO error. exe path is not found.
	)
	@IF %L_ERROR_EXE_PATH% == 3 (
		@ECHO error. exe path is directry.
	)

	@rem argument input directry error.
	@IF %L_ERROR_INPUTDIR% == 1 (
		@ECHO error. not argument input path.
	)
	@IF %L_ERROR_INPUTDIR% == 2 (
		@ECHO error. input path is not found.
	)
	@IF %L_ERROR_INPUTDIR% == 3 (
		@ECHO error. input path is not directry.
	)

	@rem argument output directry error.
	@IF %L_ERROR_OUTPUTFILE% == 1 (
		@ECHO error. not argument output path.
	)
	@IF %L_ERROR_OUTPUTFILE% == 2 (
		@ECHO error. output path is not file.
	)

	@rem argument no error.
	@SET L_ERROR_CODE=%L_ERROR_EXE_PATH%^-%L_ERROR_INPUTDIR%^-%L_ERROR_OUTPUTFILE%^-%L_ERROR_EXE_LOCAL%
	@IF "%L_ERROR_CODE%" == "0-0-0-0" (
		@rem no error.
		@SET NSTHAC_INPUTDIR=%L_ARG_INPUTDIR%
		@SET NSTHAC_OUTPUTFILE=%L_ARG_OUTPUTFILE%
		@ECHO. | CALL "%L_ARG_EXE_PATH%"
	) ELSE (
		@rem error.
		@ECHO.
		@ECHO ^(error code. %L_ERROR_CODE%^)
		@ECHO.
		@CALL :l_out_help
	)

	@ECHO.
	@ECHO end process.
@GOTO l_end


@rem //---------------------------------------------------
@rem // endpoint.
@rem //---------------------------------------------------
:l_end
@ENDLOCAL