@SETLOCAL
@ECHO OFF
echo.
echo [arg 0]
@CALL :out %0
echo [arg 1]
@CALL :out %1
echo [arg 2]
@CALL :out %2
@GOTO :EOF

:out
	@rem drive
	echo -^> %~d1
	@rem current directry path with out drive letter
	echo -^> %~p1
	@rem file name or directry name
	echo -^> %~n1
	@rem extention
	echo -^> %~x1
	@rem current directry path
	echo -^> %~dp1
	@rem fname
	echo -^> %~nx1
@GOTO :EOF


@ENDLOCAL