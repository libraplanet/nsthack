#pragma warning(disable: 4141)
#define _CRT_SECURE_NO_WARNINGS
#define DllExport __declspec(dllexport)

#include <windows.h>
#include <ShlObj.h>
#include <stdio.h>
#include <stdlib.h>

#define L_ENV "NSTHAC_INPUTDIR"

/**
 * SHGetPathFromIDList
 * get select folder path from id.
 */
DllExport SHSTDAPI_(BOOL) SHGetPathFromIDListA(LPCITEMIDLIST pidl, LPSTR pszPath)
{
	char *str = getenv(L_ENV);
	if(str == NULL)
	{
		return FALSE;
	}
	else
	{
		sprintf(pszPath, getenv(L_ENV));
		return TRUE;
	}
}

/**
 * SHBrowseForFolder
 * select input directry at dialog. 
 */
DllExport SHSTDAPI_(LPITEMIDLIST) SHBrowseForFolderA(LPBROWSEINFOA lpbi)
{
	if(getenv(L_ENV) == NULL)
	{
		return (LPITEMIDLIST)NULL;
	}
	else
	{
		return (LPITEMIDLIST)1;
	}
}